try { require('@skatejs/ssr/register') } catch(ex) { }
(function() {
	class Relative extends HTMLElement {
		// initial setup
		constructor() {
			super()
			
			// add behaviors
			for(let behavior of this.constructor.behaviors) {
				if(typeof this[behavior.name] == "function")
					this[behavior.name](...arguments)
			}
		}
		
		// Initialize component
		static define({ name, constructor, definition }) {
			if(definition == undefined) {
				// if no name defined, find out a name based on the class
				if(name === undefined)
					name = constructor.name.replace(/\.?([A-Z]+)/g, (x,y) => `-${y.toLowerCase()}`).replace(/^-/, "")

				// save it's name to "is" property
				constructor.is = name

				// save the class itself
				constructor.class = constructor
				
				// find out where we are executing the script
				try {
					constructor.here = (
							document.currentScript.ownerDocument 
						||	document
					).querySelector("[component='" + name + "']")
				} catch(ex) { }

				// behaviors
				for(let behavior of constructor.behaviors) {
					Relative.compose(constructor, behavior)
				}

				//constructor.definition = definition

				// define as an window customElement
				window.customElements.define(
					name,
					constructor
				)
			}
			else {
				// define constructor
				constructor = class extends RelativeWithBehaviors { }

				for(let [key, value] of Object.entries(definition)) {
					if(typeof value == "function")
						value = value()

					for(
						let prototype
						of [
							//constructor.prototype, // instance
							constructor.prototype.constructor// static
						]
					) {
						Object.defineProperty(
							prototype,
							key,
							{
								get: function() {
									return value
								}
							}
						)
					}
				}

				Relative.define({
					name: name,
					constructor: constructor
				})
			}

			return constructor
		}

		// Create element from html string, tag or template
		static create(html) {
			//console.log("create", html)
			if(html instanceof Element) {
				switch(html.tagName) {
					case "STYLE":
						let style = Relative.create("style")
						style.innerText = html.innerText
						return style
						break

					default:
						if(!!html.content)
							return document.importNode(html.content, true)
						else
							return Relative.create(html.outerHTML)
						break
				}
			}
			else if(!!html) {
				html = html.trim()// remove spaces

				// if it's a tag, create a fragment
				if(html.startsWith("<")) {
					var	i
						,a=document.createElement("div")
						,b=document.createDocumentFragment();
						a.innerHTML=html;
						
					// bacause table elements do not get appended to divs
					/*
						if(a.innerHTML == "") {
							console.log("test")
							a.innerHTML = "<template>" + html + "</template>"
							a = a.querySelector("template")
							console.log(a.outerHTML)
							
						}
						
					*/
					
					// append all children to a fragment
						while(i = a.firstChild) {
							//console.log(i)
							b.appendChild(i);
						}
						
						//console.log(b.childNodes)
						
						
					// the result should be something, right?
					if(b.childNodes.length > 0) {
						// templates are not returned directly
						if(b.childNodes && b.childNodes[0].tagName == "TEMPLATE")
							return Relative.create(b.childNodes[0])
						else
							// if nothing went wrong, here's the resulintg NodeList
							//return b // it would be nice if we could just return the fragment
							return 	b.childNodes.length == 1
									?	b.childNodes[0]
									:	b.childNodes
					}
					else { 
						throw "Relative could not create " + html
						// this can happen because some elements only work inside table elements
					}
				}
				// if it's a tag name, call itself
				else {
					return document.createElement(html)
					//return Relative.create(`<${html}>`)
				}
			}
		}

		// observe for changes
		static observe({ name, element, callback, options }) {
			// create observers property if it doesn't exist
			if(element.observers === undefined)
				element.observers = {}
				
			// disconnect it there's a watcher with the same name
			if(element.observers[name])
				element.observers[name].disconnect()

			// create new watcher
			element.observers[name] = new MutationObserver(callback)

			// start watching
			element.observers[name]
				.observe(
					element,
					options
				)
		}
		
		// multiple class inheritance
		static compose(_target, _source) {
			// Copies the properties from one class to another
			function copy(_target, _source) {
				for (let key of Reflect.ownKeys(_source)) {
					if	(
							key !== "constructor"
						&&	key !== "prototype"
						&&	key !== "name"
					) {
						let desc = Object.getOwnPropertyDescriptor(_source, key)
						
						// only inherit if your class doesn't implement it
						if(_target[key] == undefined)
							Object.defineProperty(_target, key, desc)
					}
				}
			}
			copy(_target, _source);
			copy(_target.prototype, _source.prototype);
		}

		// for old browsers specific fixes
		static get polyfill() {
			return {
				"custom_elements": !('registerElement' in document),
				"html_imports": ('import' in document.createElement('link')),
				"template": ('content' in document.createElement('template')),
				"shadow_dom": !(document.createElement('div').attachShadow({mode: "open"}).constructor.name == "ShadowRoot")
			}
		}


		static parse(obj) {
			let data = null
			// try to convert if it's an object
			try { data = eval(`(${obj})`) }
			catch (ex){	data = obj }

			return data
		}

		static stringify(obj, prop) {
			var placeholder = '____PLACEHOLDER____';
			var fns = [];
			try {
				var json = JSON.stringify(
					obj, function(key, value) {
					if (typeof value === 'function') {
						fns.push(value);
						return placeholder;
						}
						return value;
					}, 2);
					json = json.replace(new RegExp('"' + placeholder + '"', 'g'), function(_) {
						return fns.shift();
					}
				);
				
			} catch(ex) { 
				// console.log(ex)
				// return []
				return undefined
			}
			//return 'this["' + prop + '"] = ' + json + ';';
			return json
		}
	}

	class RelativeShadow {
		RelativeShadow() {
			this.attachShadowDOM()
		}
		
		attachShadowDOM() {
			this.attachShadow({ mode: 'open' })
		}
	}

	class RelativeState {
		RelativeState() {
			this.attachState()
		}
		
		// can be used when it looses state while cloning
		attachState() {
			this.state = new Proxy(
				this,
				{
					get: function(target, name){
						if(typeof name == "string") {
							// if it has the attribute defined
								
							let data = undefined
							if(target.hasAttribute(name))
								// try to convert if it's an object
								try { data = eval(`(${target.getAttribute(name)})`) }
								catch (ex){	data = target.getAttribute(name) }
							

							return data
						}
					},
					set: function(target, name, value) {
						

						let new_value
						// convert data to valid attribute value
						if(typeof value === 'object')
							new_value = //JSON.
										Relative.stringify(value)
						else
							new_value = value

						// preventing changing what didn't change, which causes mutationobserver calls
						if(target.getAttribute(name) != new_value) {
							target.setAttribute(name, new_value)
						}
						
						return true
					}
				}
			)
		}
	}

	class RelativeSnippets {
		RelativeSnippets() {
			//if(typeof module == 'undefined') {
				// add template, style and imports to the current element
				for(let el of Object.values(this.snippets)) {
					//console.log(Relative.create(el))
					if(!!el) {
						this.shadowRoot.appendChild(Relative.create(el))
					}
				}
			//}
		}
		
		get snippets() {
			if(typeof module == 'undefined') {
				return {
					template: this.template,
					styles: this.styles,
					imports: this.imports
				}
			}
			else {
				return { }
			}
		}
		
		/*
		get snippets() {
			return { }
		}
		*/
		
		get template() {
			let template = (this.getComponentSnippet("template#" + this.constructor.is) || this.getComponentSnippet("template"))

			if(template == undefined) {
				return `
					<template>
						<slot></slot>
					</template>
				`
			}

			return template
		}
		
		get styles() {
			let style = (this.getComponentSnippet("style#" + this.constructor.is) || this.getComponentSnippet("style"))

			if(Relative.polyfill.shadow_dom)
				style.innerText = style.innerText.replace(/:host/g, this.constructor.is)

			return style
		}
		
		get imports() {
			return (this.getComponentSnippet("imports#" + this.constructor.is) || this.getComponentSnippet("imports"))
		}
		
		getComponentSnippet(tag) {
			try{
				return this.constructor.here.querySelector(tag) || undefined
			}
			catch(ex){
				return undefined
			}
		}
	}

	class RelativeLifecycle {
		RelativeLifecycle() {
			const fix_callback = (callback) => {
				try {
					return eval(`(function ${callback})`).bind(this)
				} catch(ex) {
					return callback
				}
			}

			// attach lifecycle methods to the object
				let lifecycle = null
				for(let [event, callback] of Object.entries(this.constructor.lifecycle)) {
					//console.log(callback)
					//let new_callback = 
					//this[event] = callback.bind(this)
					this[event] = fix_callback(callback)
				}

			// bind scope to custom lifecycles
				for(let [name, func] of Object.entries(this.constructor.lifecycle)) {
					switch(typeof func) {
						case "function":
							//this.constructor.lifecycle[name] = func.bind(this)
							//this.constructor.lifecycle[name] = fix_callback(func)
							break

						case "object":
							//this.constructor.lifecycle[name].callback = func.callback.bind(this)
							//this.constructor.lifecycle[name].callback = fix_callback(func.callback)
							break
					}
				}
		}
		
		static get lifecycle() {
			return {}
		}

		connected() { }
		connectedCallback() {
			//console.log(this.outerHTML)
			this._RelativeAttributes()
			this.connected()
		}
		
		disconnected() { }
		disconnectedCallback() {
			this.disconnected()
		}
		
		changed() { }
		attributeChangedCallback() {
			this.changed(...arguments)
			//console.log("CHANGED", arguments)
			this.try_activate(arguments[0])
		}
		
		adopted() { }
		adoptedCallback() {
			this.adopted()
		}
		
		try_activate(param) {
			let activated = Object.entries(this.constructor.attributes).reduce(
				(memo, current) => {
					let [name, def] = current
					
					if(def.activates) {
						if(!this.hasAttribute(name)) {
							memo = false
						}
					}

					if(name == param && def.activates != true) {
						memo = false
					}
					
					return memo
				},
				true
			)
			
			if(activated) {
				let event = this.getLifecycleEvent('activated')
				if(event != undefined)
					event.bind(this)()
			}
		}

		getLifecycleEvent(name) {
			let event = this.constructor.lifecycle[name]
			switch(typeof event) {
				case "object":
					if(typeof event.callback != "undefined")
						return event.callback
					else
						return function() { }
					break

				case "function":
					return event
					break
			}
		}
		
		activated() {
			
		}
	}


	class RelativeAttributes {
		_RelativeAttributes() {
			for(let [field, config] of Object.entries(this.constructor.attributes)) {
				let defaultv = config.default
					
				// default
					if(!this.hasAttribute(field))
					//if(this.state[field] == undefined) 
					{
						// console.log("default", defaultv)
						
						if(defaultv != undefined) {
							this.state[field] = defaultv
						}
					}
			}

			//if(false)
			for(let [field, config] of Object.entries(this.constructor.attributes)) {
				if(this.state[field] != undefined) {
					this.changed(field, undefined, this.state[field])
				}
			}
		}

		changed(field, old_value, new_value) {
			let attribute = this.constructor.attributes[field]

			if(attribute) {
				let handler = attribute.changed
				if(handler != undefined) {
					handler.bind(this)(new_value, old_value)
				}
			}
		}

		// automatically observe defined properties
		static get observedAttributes() {
			return Object.keys(this.attributes)
		}

		static get attributes() {
			return { }
		}
	}

	class RelativeListeners {
		RelativeListeners() {
			for(let [event, callback] of Object.entries(this.constructor.listeners)) {
				this.addEventListener(event, callback.bind(this))
			}
		}
		
		static get listeners() {
			return { }
		}	
	}

	class RelativeMethods {
		RelativeMethods() {
			let aux = {}

			if(this.constructor.methods)
			for(let [name, func] of Object.entries(this.constructor.methods)) {
				aux[name] = func.bind(this)
			}

			this.methods = aux
		}

		static get methods() {
			return { }
		}
	}

	class RelativeNodes {
		RelativeNodes() {
			// internal solution to avoid implementation of querySelector on ssr
			function getChildByName(context, name) {
				for(let child of context.childNodes) {
					if(child.getAttribute("name") == name) {
						return child
					}
				}
			}
			
			// create nodes
			for(let [name, node] of Object.entries(this.constructor.nodes)) {
				let el = Relative.create(`<${node.node} name='${name}'>`)
				//console.log(name, el)
				
				this.appendChild(el)
			
			}
			
			// create observers for the links
			for(let [name, node] of Object.entries(this.constructor.nodes)) {
				if(node.links != undefined)
				for(let link of node.links) {
					let observer = getChildByName(this, name)
					let observee = getChildByName(this, link.node)
					
					Relative.observe({
						name: `${name}_${link.node}_${link.output}_${link.input}`,
						element: observee,
						callback: function(mutations) {
							if(false)
							console.log("\ntest\n")
							mutations.map((current) => {
								if(false)
								console.log({
									oatt: current.attributeName,
									latt: link.output,
									observer: observer.outerHTML,
									observee: observee.outerHTML
								})
								if(current.attributeName == link.output) {
									observer.state[link.input] = observee.state[link.output]
								}
							})
						},
						options: {
							attributes: true
						}
					})
				}
			}
			
			// set attributes to the network nodes
			for(let [name, node] of Object.entries(this.constructor.nodes)) {
				if(node.attributes)
				for(let [attr, value] of Object.entries(node.attributes)) {
					let el = getChildByName(this, name)
					
					el.state[attr] = value
				}
			}
		}
		
		static get nodes() {
			return { }
		}
	}

	// allow to set debug messages and use only when you ask for it
	class RelativeDebug {
		
	}

	class RelativeWithBehaviors extends Relative {
		static get behaviors() {
			return [
				RelativeShadow,
				RelativeState,
				RelativeSnippets,
				RelativeAttributes,
				RelativeLifecycle,
				RelativeListeners,
				RelativeMethods,
				RelativeNodes
			]
		}
	}



	Relative = RelativeWithBehaviors


	/* exports */
		try {
			module.exports = Relative
		} catch(ex) { }

		try {
			global.Relative = Relative 
		} catch(ex) { }

		try {
			window.Relative = Relative
		} catch(ex) { }



})()
